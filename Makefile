CFLAGS+=-std=c99 -Wall -pedantic -Wextra
LDFLAGS+=

PDFTEX=pdftex
BUILD=build


.PHONY: all clean

all: $(BUILD)/elide $(BUILD)/elide.pdf

$(BUILD)/elide: $(BUILD)/elide.c
	$(CC) -o $(BUILD)/elide $(BUILD)/elide.c $(CFLAGS) $(LDFLAGS)

$(BUILD)/elide.c: elide.w $(BUILD)
	$(CTANGLE) elide.w - $(BUILD)/elide.c

$(BUILD)/elide.pdf: $(BUILD)/elide.tex
	cd $(BUILD); $(PDFTEX) elide.tex

$(BUILD)/elide.tex: elide.w $(BUILD)
	$(CWEAVE) elide.w - $(BUILD)/elide.tex

$(BUILD):
	mkdir -p $(BUILD)

clean:
	rm -rf build
