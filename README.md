# ELIDE
This is a Literate focused editor aimed to be an _IDE_ in the end. The idea
is for the editor to be keyboard centric and taylored to my needs.

## Building
### Requeriments
In order to build this program you will need
- a `C` compiler
- `make`
- `CWEB`

Also, you will need some backend for the GUI. At the moment the only
implemented one is `X11`. If required, more ports will come.

### Build instructions
Simply run
```
$ make
```

and the result will be in a folder called `build`.
