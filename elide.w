@* Hello World. This is a hello world program, it will hopefully evolve with time

@p
@<includes@>@;

int
main(int argc, char *argv[])
{
	(void) argc;
	(void) argv;

	printf("Hello world!\n");
	return 0;
}

@ here we define all includes, at the time being we only need {\it stdio.h}.
@<includes@>=
#include <stdio.h>


